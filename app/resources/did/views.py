from flask import Blueprint, jsonify, request
from flask_jwt_extended import jwt_required
from .models import DID
from .schemas import DIDSchema
from app.utils.db import db

did_bp = Blueprint('did', __name__)
did_schema = DIDSchema()

@did_bp.route('/<int:pk>', methods=['GET'])
@jwt_required
def detail(pk: str):
    """Find one DID based in the pk
    Args:
        pk: Primary key of a DID
    Returns:
        JSON representation of a single DID OR 500 status code if something unexpected happens
    """
    try:
        did = DID.get_by_id(pk)
        if not did:
            return jsonify(error=f'The DID with id "{pk}" was not found'), 404

        data = did_schema.dump(did)

        return jsonify({'data': data, 'message': "successfully fetched"}), 200
    except Exception:
        ## this should be logged in a production environment with sentry/log tool
        return (
            jsonify(
                error='An unexpected error has occurred, please try again in a few minutes'
            ),
            500,
        )


@did_bp.route('/', methods=['GET'])
@jwt_required
def get_many():
    """Returns a collection of DIDs based on the page and page size
    Args (Query String):
        page: Integer number that represents a page in the paginate provided by paginate technique
        per_page: Integer number that represents the page size (number of items)
    Returns: JSON with a list of items, number of page, flag to indicate if exists a next page and the total of items 
            OR 500 status code if something unexpected happens
    """
    try:
        per_page = 10
        page = 1
        if 'page' in request.args:
            page = request.args['page']
            if not page.isdigit():
                return (
                    jsonify(error='The page argument must be a positive digit'),
                    400,
                )
            page = int(page)

        if 'per_page' in request.args:
            per_page = request.args['per_page']
            if not per_page.isdigit():
                return (
                    jsonify(error='The per_page argument must be a positive digit'),
                    400,
                )
            per_page = int(per_page)

        dids = DID.get_by_page(page=page, page_size=per_page)
        data = did_schema.dump(dids.items, many=True)

        full_response = {
            'data': data,
            'has_next_page': dids.has_next,
            'next_page_num': dids.next_num,
            'total': dids.total,
            'message': "successfully fetched"
        }

        return jsonify(full_response), 200
    except Exception:
        ## this should be logged in a production environment with sentry/log tool
        return (
            jsonify(
                error='An unexpected error has occurred, please try again in a few minutes'
            ),
            500,
        )


# Would be good if this route have some Rate limit based on the client/IP?
@did_bp.route('/', methods=['POST'])
@jwt_required
def create():
    """ Create a DID record
    Args:
        JSON with the DID's required data
    Returns: The DID created and a message of success OR
                Error message of a invalid JSON DID (400)
                Error message of a exception (500)
    """
    try:
        errors = did_schema.validate(request.get_json())
        if errors:
            return jsonify(errors=errors), 400

        data = did_schema.load(request.get_json())

        did = DID(**data).save()
        result = did_schema.dump(did)
        
        return jsonify({'data':result, 'message': 'successfully created'}), 201
    except Exception:
        ## this should be logged in a production environment with sentry/log tool
        db.session.rollback()
        return (
            jsonify(
                error='An unexpected error has occurred, please try again in a few minutes'
            ),
            500,
        )


@did_bp.route('/<int:pk>', methods=['PUT'])
@jwt_required
def update(pk: int):
    """Update a DID's record
    Args:
        JSON with the DID's required data
    Returns: The DID updated and a message of success OR
                Error message of a invalid JSON DID (400)
                Error message of a exception (500)
    """
    try:
        did = DID.get_by_id(pk)
        if not did:
            return jsonify(error=f'The DID with id "{pk}" was not found'), 404

        errors = did_schema.validate(request.get_json())
        if errors:
            return jsonify(errors=errors), 400

        data = did_schema.load(request.get_json())

        for key, value in data.items():
            setattr(did, key, value)
        db.session.commit()

        result = did_schema.dump(did)

        return jsonify(result), 200
    except Exception:
        ## this should be logged in a production environment with sentry/log tool
        db.session.rollback()
        return (
            jsonify(
                error='An unexpected error has occurred, please try again in a few minutes'
            ),
            500,
        )


@did_bp.route('/<int:pk>', methods=['DELETE'])
@jwt_required
def remove(pk: int):
    """Remove a specific DID based on pk (ID)
    Args:
        pk: primary key of the record to be removed
    Returns: 
    """
    try:
        did = DID.get_by_id(pk)
        if not did:
            return jsonify(error=f'The DID with id "{pk}" was not found'), 404

        did.remove()
        
        return jsonify({'data': {}, 'message': 'successfully deleted'}), 204
    except Exception as e:
        raise e
        ## this should be logged in a production environment with sentry/log tool
        return (
            jsonify(
                error='An unexpected error has occurred, please try again in a few minutes'
            ),
            500,
        )