from __future__ import annotations
from datetime import datetime
from typing import List

from app.utils.db import db

class DID(db.Model):
    """
        DID model 
    """

    __tablename__ = 'dids'

    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(20), unique=True, nullable=False)
    monthy_price = db.Column(db.Numeric(precision=15, scale=2), nullable=False)
    setup_price = db.Column(db.Numeric(precision=15, scale=2), nullable=False)
    currency = db.Column(db.String(3), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now)

    @classmethod
    def get_by_id(cls, pk: int) -> DID:
        return DID.query.filter_by(id=pk).first()

    @classmethod
    def get_by_start_and_size(cls, start: int, size: int) -> List[DID]:
        return DID.query.order_by('created_at').offset(start).limit(size).all()

    @classmethod
    def get_by_page(cls, page: int, page_size: int):
        return DID.query.order_by('created_at').paginate(page=page, per_page=page_size)
        
    @classmethod
    def get_by_value(cls, value: str) -> DID:
        return DID.query.filter_by(value=value).first()

    def remove(cls):
        ## Unless autocommit=True, a transaction is started automatically
        db.session.delete(cls)
        db.session.commit()
        return cls
    
    def save(cls):
        ## Unless autocommit=True, a transaction is started automatically
        db.session.add(cls)
        db.session.commit()
        return cls

    