from flask import Blueprint, jsonify, request
from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_refresh_token_required, get_jwt_identity, 
                                fresh_jwt_required)

from .models import User
from .schemas import RegisterSchema, LoginSchema

auth_bp = Blueprint('auth', __name__)
login_schema = LoginSchema()
register_schema = RegisterSchema()

@auth_bp.route('/login', methods=['POST'])
def login():
    """ Login a user
    Args:
        JSON with username and password
    Returns: 
        JWT Token and refresh token
    """
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    errors = login_schema.validate(request.get_json())
    if errors:
        return jsonify(errors=errors), 400

    username = request.json['username']
    password = request.json['password']
    
    user = User.find_by_username(username)
    
    if user and check_password_hash(user.password, password):
        access_token = create_access_token(identity=user.username, fresh=True)
        return jsonify(access_token=access_token, refresh_token=create_refresh_token(identity=username)), 200

    return jsonify({"message": "Bad username or password"}), 401

@auth_bp.route('/register', methods=['POST'])
def register():
    """ Register a User
    Args: 
        JSON with username and password
    Returns: 
        JSON with success message and the username registered OR
        Error with a message
    """
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    errors = register_schema.validate(request.get_json())
    if errors:
        return jsonify(errors=errors), 400

    username = request.json['username']
    password = request.json['password']

    pass_hash = generate_password_hash(password)
    try:
        user = User(username, pass_hash).save()
        result = register_schema.dump(user)
        return jsonify({'message': 'successfully registered', 'data': {'username': result['username']}}), 201
    except Exception:
        # this exception should be logged in a tool like sentry
        return jsonify({'message': 'unable to create', 'data': {}}), 500

