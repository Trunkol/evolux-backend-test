from app.utils.db import db

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(255))

    def __init__(cls, username, password):
        cls.username = username
        cls.password = password

    def save(cls):
        db.session.add(cls)
        db.session.commit()
        return cls

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()
