# -*- coding: utf-8 -*-

from marshmallow import Schema, fields, ValidationError, validate
from .models import User

class RegisterSchema(Schema):
    """
        Represents the expected payload to register user
    """
    def validate_unique_username(value):
        user = User.find_by_username(value)
        if user:
            raise ValidationError(f'user already exists {user.username}')

    username = fields.String(required=True, validate=[validate_unique_username, validate.Length(min=1)])
    password = fields.String(required=True, validate=validate.Length(min=1))

class LoginSchema(Schema):
    """
        Represents the expected payload to login user
    """
    username = fields.String(required=True, validate=validate.Length(min=1))
    password = fields.String(required=True, validate=validate.Length(min=1))