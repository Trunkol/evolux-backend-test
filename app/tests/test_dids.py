
import random

from typing import Any, Callable, Dict, List
from unittest import mock
from flask import Flask
from flask.testing import FlaskClient
from app.resources.did.models import DID

class TestDidsWithoutAuth(object):
    def test_should_not_access_find_one_did_unauthenticated(self,
        app: Flask,
        client: FlaskClient,
    ):
        with app.test_request_context():
            res = client.get('/api/v1/did/49348493')
            assert res.status_code == 401
    
    def test_should_not_access_find_a_page_of_dids_unauthenticated(self,
        app: Flask,
        client: FlaskClient,
    ):
        with app.test_request_context():
            res = client.get('/api/v1/did/?page=1')
            assert res.status_code == 401
    
    def test_should_not_create_a_did_unauthenticated(self,
        app: Flask,
        client: FlaskClient,
    ):
        with app.test_request_context():
            res = client.post('/api/v1/did/', json={"id":42,"value":"+55 84 91234-4321","monthyPrice":"0.03","setupPrice":"3.40","currency":"U$"})
            assert res.status_code == 401

class TestDidsWithAuth(object):
    def test_should_get_one_specif_did(
        self, app: Flask, auth_client: FlaskClient, many_dids: List[DID]
    ):
        with app.test_request_context():
            res = auth_client.get('/api/v1/did/1')
            assert res.status_code == 200
            assert 'data' in res.get_json()            

    def test_should_get_one_specific_dids_page(
        self, app: Flask, auth_client: FlaskClient, many_dids: List[DID]
    ):
        with app.test_request_context():
            res = auth_client.get('/api/v1/did/?page=1')
            assert res.status_code == 200
            assert 'data' in res.get_json()            
            assert 'has_next_page' in res.get_json()
            assert 'next_page_num' in res.get_json()
            assert len(many_dids) == res.get_json()['total'] 
    
    def test_should_failed_in_remove_a_did(
        self, app: Flask, auth_client: FlaskClient, many_dids: List[DID]
    ):
        with app.test_request_context():
            res = auth_client.delete('/api/v1/did/804/')
            assert res.status_code == 404

    def test_should_create_a_did(
        self, app: Flask, auth_client: FlaskClient
    ):
        with app.test_request_context():
            payload = {"value":"+55 84 91234-4321","monthyPrice":"0.03","setupPrice":"3.40","currency":"U$"}
            res = auth_client.post('/api/v1/did/', json=payload)
            assert res.status_code == 201
            assert 'data' in res.get_json()
                
    def test_should_get_a_error_creating_a_did_with_empty_value(
        self, app: Flask, auth_client: FlaskClient
    ):
        with app.test_request_context():
            payload = {"value":"","monthyPrice":"0.03","setupPrice":"3.40","currency":"U$"}
            res = auth_client.post('/api/v1/did/', json=payload)
            assert res.status_code == 400
            assert 'errors' in res.get_json()
                
    def test_should_get_a_error_creating_a_did_with_a_negative_monthyprice(
        self, app: Flask, auth_client: FlaskClient
    ):
        with app.test_request_context():
            payload = {"value":"+55 84 91234-4321","monthyPrice":"-0.03","setupPrice":"3.40","currency":"U$"}
            res = auth_client.post('/api/v1/did/', json=payload)
            assert res.status_code == 400
            assert 'errors' in res.get_json()
                
    def test_should_get_a_error_creating_a_did_with_a_negative_setupprice(
        self, app: Flask, auth_client: FlaskClient
    ):
        with app.test_request_context():
            payload = {"value":"+55 84 91234-4321","monthyPrice":"0.03","setupPrice":"-3.40","currency":"U$"}
            res = auth_client.post('/api/v1/did/', json=payload)
            assert res.status_code == 400
            assert 'errors' in res.get_json()
                
    def test_should_get_a_error_creating_a_did_without_value(
        self, app: Flask, auth_client: FlaskClient
    ):
        with app.test_request_context():
            payload = {"monthyPrice":"0.03","setupPrice":"-3.40","currency":"U$"}
            res = auth_client.post('/api/v1/did/', json=payload)
            assert res.status_code == 400
            assert 'errors' in res.get_json()