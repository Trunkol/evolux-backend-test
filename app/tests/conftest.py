# -*- coding: utf-8 -*-

from flask import Flask
from flask_jwt_extended import create_access_token
from flask_sqlalchemy import SQLAlchemy
import pytest

from flask.helpers import url_for
from app.utils.db import db as _db
from app.resources.auth.models import User
from app.resources.did.models import DID

@pytest.fixture(scope='session')
def app():
    """
    the app instance that will be used in tests
    """
    from app.main import app
    return app


@pytest.fixture(scope='session')
def client(app: Flask):
    """
    this client should work in the session scope and will be available in tests
    """
    return app.test_client()

@pytest.fixture(scope='session')
def auth_client(app: Flask, access_token: str):
    """
    this auth client with a JWT token will be available in tests as auth_client
    """
    client = app.test_client()
    client.environ_base['HTTP_AUTHORIZATION'] = f'JWT {access_token}'
    return client


@pytest.fixture(scope='session', autouse=True)
def db(app: Flask):
    """
    Creates the tests db injectable in session level
    """
    with app.app_context():
        _db.create_all()

        yield _db

        _db.drop_all()


@pytest.fixture(scope='session')
def user(db: SQLAlchemy):
    """
    Creates an user that could be used in the tests
    """
    password = '123456'
    user = User(username='superuser', password=password)
    db.session.add(user)
    db.session.commit()

    return {
        'id': user.id,
        'username': user.username,
        'password': password,
    }

@pytest.fixture(scope='session')
def auth_register_url(app):
    """
    URL used to access the auth resource register action
    """
    with app.test_request_context():
        return '/api/v1/auth/register'


@pytest.fixture(scope='session')
def auth_login_url(app):
    """
    URL used to access the auth resource login action
    """
    with app.test_request_context():
        return '/api/v1/auth/login'

@pytest.fixture(scope='session')
def many_dids(db: SQLAlchemy):
    """
    Creates some DID's to use in the tests
    """
    dids = []

    for n in range(800):
        dids.append(DID(
            value=f'+55 84 {n:03}12:1203',
            monthy_price=0.03,
            setup_price=3.40,
            currency='USD',
        ))
        
    db.session.bulk_save_objects(dids)
    db.session.commit()

    return dids

@pytest.fixture(scope='session')
def access_token(app: Flask, user: User):
    with app.test_request_context():
        u = User.find_by_username(user['username'])
        return create_access_token(identity=u.username)