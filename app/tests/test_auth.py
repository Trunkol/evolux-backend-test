# -*- coding: utf-8 -*-

from flask import Flask
from flask.testing import FlaskClient
from flask_sqlalchemy import SQLAlchemy
from typing import Dict, Any

from app.resources.auth.models import User

class TestRegister(object):
    def test_should_return_an_error_when_payload_is_invalid(
        self,
        app: Flask,
        client: FlaskClient,
        user: Dict[str, Any],
        auth_register_url: str,
    ):
        with app.test_request_context():
            payloads = (
                {'username': '', 'password': '112233'},
                {'username': ''}, 
                {'password': '112233'},
                {'username': 'test', 'password': ''},
            )
            for payload in payloads:
                res = client.post(auth_register_url, json=payload)
                assert res.status_code == 400

    def test_should_return_the_access_token_when_register_successfully(
        self,
        app: Flask,
        client: FlaskClient,
        user: Dict[str, Any],
        auth_register_url: str,
    ):
        with app.test_request_context():
            payload = {'username': f'{user["username"]}new', 'password': '654321'}
            res = client.post(auth_register_url, json=payload)
            assert res.status_code == 201
            

class TestLogin(object):
    def test_should_return_errors_when_payload_is_invalid(
        self,
        app: Flask,
        client: FlaskClient,
        user: Dict[str, Any],
        auth_login_url: str,
    ):
        with app.test_request_context():
            payloads = (
                'invalid',
                {'username': user['username']},
                {'password': user['password']},
            )
            for payload in payloads:
                res = client.post(auth_login_url, json=payload)
                assert res.status_code == 400

    def test_should_return_an_error_when_credentials_doesnt_matches(
        self,
        app: Flask,
        client: FlaskClient,
        user: Dict[str, Any],
        auth_login_url: str,
    ):
        with app.test_request_context():
            payloads = (
                {'username': user['username'], 'password': f'{user["password"]}0'},
                {
                    'username': f'{user["username"]}unexisting',
                    'password': user['password'],
                },
            )
            for payload in payloads:
                res = client.post(auth_login_url, json=payload)
                assert res.status_code == 401

class TestIntegrationRegisterLogin(object):
    def test_should_register_a_user_and_login_correctly(
        self,
        app: Flask,
        client: FlaskClient,
        auth_login_url: str,
        auth_register_url: str,
    ):
        with app.test_request_context():
            payload = {'username': 'test1', 'password': '123'}    
            res = client.post(auth_register_url, json=payload)
            assert res.status_code == 201
            
            res = client.post(auth_login_url, json=payload)
            assert res.status_code == 200
            assert 'access_token' in res.get_json()            
            
    def test_should_register_a_user_and_login_failed(
        self,
        app: Flask,
        client: FlaskClient,
        auth_login_url: str,
        auth_register_url: str,
    ):
        with app.test_request_context():
            payload = {'username': 'test2', 'password': '123'}
            res = client.post(auth_register_url, json=payload)
            assert res.status_code == 201

            payload = {'username': 'test2', 'password': '1234'}
            res = client.post(auth_login_url, json=payload)
            assert res.status_code == 401
