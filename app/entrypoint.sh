#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "[INFO] Iniciando postgres..."

    while ! nc -z 'db' '5432'; do
      sleep 1
    done

    echo "[INFO] PostgreSQL Iniciado"
fi

exec "$@"

echo "[INFO] Rodando as migrações"
flask init-db
exec "$@"
