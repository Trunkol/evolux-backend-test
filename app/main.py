import os

from flask import Flask, current_app
from flask_jwt_extended import JWTManager

from .resources.auth.views import auth_bp
from .resources.did.views import did_bp
from .utils.db import db
from .settings import CONFIG_NAME_MAPPER

# create and configure the app
app = Flask(__name__, instance_relative_config=True)

@app.cli.command("init-db")
def init_db():
    db.drop_all()
    db.create_all()

env_flask_env = os.getenv('FLASK_ENV')

if env_flask_env:
    app.config.from_object(CONFIG_NAME_MAPPER[env_flask_env])
else:
    app.config.from_object(CONFIG_NAME_MAPPER['testing'])

jwt = JWTManager(app)

app.register_blueprint(auth_bp, url_prefix='/api/v1/auth') 
app.register_blueprint(did_bp, url_prefix='/api/v1/did') 

db.init_app(app)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=80)
