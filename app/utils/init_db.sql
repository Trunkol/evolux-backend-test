DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS dids CASCADE;
/*

If you can't use db.create_all() provided by SQLAlchemy, try:

CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE dids (
  id SERIAL PRIMARY KEY,
  value VARCHAR(25) NOT NULL,
  monthy_price DECIMAL(15,2) NOT NULL,
  setup_price DECIMAL(15,2) NOT NULL,
  currency VARCHAR(5) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
*/